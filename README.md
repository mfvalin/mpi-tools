# mpi-tools

diagnostic tools for MPI programs

MPI profiler :
  mpi_prof_common.c     : common routines for C and Fortran profiling
  mpi_prof_c.c          : routines for C profiling
  mpi_prof_fortran.c    : routines for Fortran profiling

N.B.: 
  for now Fortran name mangling with one added underscore is assumed in the C code

  the tests are intended to be run with 2 MPI processes

  when using gfortran, add GNU=yes to the make command
  make GNU=yes tests

  to run the tests with profiling:

  mpirun -n 2 ./f_test.Abs                                           # static

  LD_PRELOAD=$(pwd)/libmpitools_f.so mpirun -n 2 ./f_test_dyn.Abs    # with shared library

  mpirun -n 2 ./c_test.Abs                                           # static
  
  LD_PRELOAD=$(pwd)/libmpitools.so mpirun -n 2 ./c_test_dyn.Abs      # with shared library
