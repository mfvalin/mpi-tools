program test_mpi
  use ISO_C_BINDING
#if defined MPIF08
  use mpi_f08
  implicit none
  type(MPI_win) :: win1, win2, win3, win4
  INTEGER(KIND=MPI_ADDRESS_KIND) siz, disp
  type(C_PTR) :: base
  TYPE(MPI_Status) :: status
#else
  use mpi
  implicit none
!   include 'mpif.h'
  integer :: win1, win2, win3, win4
  INTEGER(KIND=MPI_ADDRESS_KIND) siz, base, disp
  integer, dimension(MPI_STATUS_SIZE) :: status
#endif
  integer, parameter :: WIN_SIZE = 1024
  integer :: ierror, disp_unit
  integer, dimension(WIN_SIZE) :: array, array3, array2
!   integer, dimension(:), pointer :: array2
  integer :: my_rank, nprocs, ne, next_rank
  type(C_PTR) :: cptr
  integer, dimension(:), pointer :: a
  external :: fdummy1, fdummy2, fdummy3

  call mpi_init(ierror)
  call MPI_At_Finalize(fdummy1)
  call MPI_At_Finalize(fdummy2)
  call MPI_At_Finalize(fdummy3)
  call mpi_comm_size(MPI_COMM_WORLD, nprocs,  ierror)
  call mpi_comm_rank(MPI_COMM_WORLD, my_rank, ierror)
  next_rank = mod(my_rank+1, nprocs)
print *,'=========== after init'

#if defined MPIF08
  call MPI_Sendrecv(array, 1, MPI_INTEGER, my_rank, 0, array2, 1, MPI_INTEGER, my_rank, 0, MPI_COMM_WORLD, status)
#else
  call MPI_Sendrecv(array, 1, MPI_INTEGER, my_rank, 0, array2, 1, MPI_INTEGER, my_rank, 0, MPI_COMM_WORLD, status, ierror)
#endif
print *,'=========== after sendrecv'

  siz = 4 * WIN_SIZE
  disp_unit = 4
  disp = 0
  call MPI_WIN_ALLOCATE(siz,  disp_unit, MPI_INFO_NULL, MPI_COMM_WORLD, base,    win1, ierror)
  cptr = transfer(base, cptr)
  call C_F_POINTER(cptr, a, [WIN_SIZE])
  call fill(a, WIN_SIZE, my_rank*WIN_SIZE)
  array = -1
  call mpi_barrier(MPI_COMM_WORLD, ierror)

  call MPI_WIN_LOCK(MPI_LOCK_SHARED, next_rank, 0, win1, ierror)
  call MPI_GET(array, WIN_SIZE, MPI_INTEGER, next_rank, disp, WIN_SIZE, MPI_INTEGER, win1, ierror)
  call MPI_WIN_UNLOCK(next_rank, win1, ierror)
  ne = errors(array, WIN_SIZE, next_rank*WIN_SIZE)
  call mpi_barrier(MPI_COMM_WORLD, ierror)
print *,'=========== MPI_WIN_ALLOCATE errors =', ne

  call MPI_WIN_ALLOCATE_SHARED(siz, disp_unit, MPI_INFO_NULL, MPI_COMM_WORLD, base, win2, ierror)
  call MPI_WIN_SHARED_QUERY(win2, 0, siz, disp_unit, base, ierror)
  cptr = transfer(base, cptr)
  call C_F_POINTER(cptr, a, [WIN_SIZE])
  if(my_rank == 0) call fill(a, WIN_SIZE, 0)
  call mpi_barrier(MPI_COMM_WORLD, ierror)
  ne = errors(a, WIN_SIZE, 0)
print *,'=========== MPI_WIN_ALLOCATE_SHARED errors =',ne

  call MPI_WIN_CREATE(array2, siz, disp_unit, MPI_INFO_NULL, MPI_COMM_WORLD, win3, ierror)
  call fill(array2, WIN_SIZE, my_rank*WIN_SIZE)
  array = -1
  call mpi_barrier(MPI_COMM_WORLD, ierror)

  call MPI_WIN_LOCK(MPI_LOCK_SHARED, next_rank, 0, win3, ierror)
  call MPI_GET(array, WIN_SIZE, MPI_INTEGER, next_rank, disp, WIN_SIZE, MPI_INTEGER, win3, ierror)
  call MPI_WIN_UNLOCK(next_rank, win3, ierror)
  ne = errors(array, WIN_SIZE, next_rank*WIN_SIZE)
print *,'=========== MPI_WIN_CREATE errors =',ne

  call MPI_WIN_CREATE_DYNAMIC(MPI_INFO_NULL, MPI_COMM_WORLD, win4, ierror)
  call MPI_WIN_ATTACH(win4, array3, siz, ierror)
  call mpi_barrier(MPI_COMM_WORLD, ierror)
  call MPI_WIN_DETACH(win4, array3, ierror)

print *,'=========== MPI_WIN_CREATE_DYNAMIC status =',ierror

  call MPI_WIN_FREE(win1, ierror)
print *,'==========='
  call mpi_barrier(MPI_COMM_WORLD, ierror)
print *,'====== after barrier ====='
1 continue
  call mpi_finalize(ierror)

  contains

subroutine test_win(base, s)
#if defined MPIF08
  type(C_PTR) :: base
#else
  INTEGER(KIND=MPI_ADDRESS_KIND) base
#endif
  integer, intent(IN) :: s
  
end subroutine test_win

subroutine fill(a, n, offset)
  integer :: n, offset
  integer, dimension(*) :: a

  integer :: i

  do i = 1, n
    a(i) = i + offset
  enddo
end subroutine fill

function errors(a, n, offset) result(ne)
  integer :: n, offset
  integer, dimension(*) :: a
  integer :: ne

  integer :: i

  ne = 0
  do i = 1, n
    if(a(i) .ne. i + offset) ne = ne + 1
  enddo
end function errors

end program

subroutine fdummy1()
  print *,'fdummy1 called'
end subroutine

subroutine fdummy2()
  print *,'fdummy2 called'
end subroutine

subroutine fdummy3()
  print *,'fdummy3 called'
end subroutine
