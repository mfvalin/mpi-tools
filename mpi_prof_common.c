//   Uuseful routines for C and FORTRAN programming
//   Copyright (C) 2020  Division de Recherche en Prevision Numerique
//                       Environnement Canada
//
//   This is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation,
//   version 2.1 of the License.
//
//   This software is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
// Author : M.Valin,   Recherche en Prevision Numerique, 2020/2021
//          V.Magnoux, Recherche en Prevision Numerique, 2020/2021
//
// common code used by C anf Fortran versions
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <mpi.h>

#if ! defined(DEBUG)
#define DEBUG 0
#endif

// static int MPI_Irecv_ctr=0;
// static int MPI_Recv_ctr=0;
// static int MPI_Isend_ctr=0;
// static int MPI_Send_ctr=0;
// static int MPI_Irecv_elem=0;
// static int MPI_Recv_elem=0;
// static int MPI_Isend_elem=0;
// static int MPI_Send_elem=0;

static struct {
  double time;  /* cumulative time spent in trapped MPI routines (seconds) */
  int    calls; /* cumulative number of calls to trapped MPI routines */
} pmpi_r_statistics = {0.0, 0};

typedef struct {
  char*     name;         /* name of tracked function */
  long long sbytes;       /* sum of bytes moved */
  double    sbytes2;      /* sum of squares for bytes moved */
  long long sbytes_coll;  /* sum of bytes * communicator_size for collectives */
  double    sbytes_coll2; /* sum of squares for collectives */
  double    time;         /* time spent in routine */
  int       calls;        /* number of calls to function*/
} stat_table_entry;

// clang-format off
static stat_table_entry stat_table[]={
  {"MPI_Init"               ,0,0.0,0,0.0,0.0,0},
  {"MPI_Init_thread"        ,0,0.0,0,0.0,0.0,0},
  {"MPI_Bcast"              ,0,0.0,0,0.0,0.0,0},
  {"MPI_Barrier"            ,0,0.0,0,0.0,0.0,0},
  {"MPI_Recv"               ,0,0.0,0,0.0,0.0,0},
  {"MPI_Irecv"              ,0,0.0,0,0.0,0.0,0},
  {"MPI_Send"               ,0,0.0,0,0.0,0.0,0},
  {"MPI_Isend"              ,0,0.0,0,0.0,0.0,0},
  {"MPI_Sendrecv"           ,0,0.0,0,0.0,0.0,0},
  {"MPI_Wait"               ,0,0.0,0,0.0,0.0,0},
  {"MPI_Testall"            ,0,0.0,0,0.0,0.0,0},
  {"MPI_Waitall"            ,0,0.0,0,0.0,0.0,0},
  {"MPI_Reduce"             ,0,0.0,0,0.0,0.0,0},
  {"MPI_Allreduce"          ,0,0.0,0,0.0,0.0,0},
  {"MPI_Gather"             ,0,0.0,0,0.0,0.0,0},
  {"MPI_Gatherv"            ,0,0.0,0,0.0,0.0,0},
  {"MPI_Scatter"            ,0,0.0,0,0.0,0.0,0},
  {"MPI_Scatterv"           ,0,0.0,0,0.0,0.0,0},
  {"MPI_Alltoall"           ,0,0.0,0,0.0,0.0,0},
  {"MPI_Alltoallv"          ,0,0.0,0,0.0,0.0,0},
  {"MPI_Get"                ,0,0.0,0,0.0,0.0,0},
  {"MPI_Put"                ,0,0.0,0,0.0,0.0,0},
  {"MPI_Accumulate"         ,0,0.0,0,0.0,0.0,0},
  {"MPI_Get_accumulate"     ,0,0.0,0,0.0,0.0,0},
  {"MPI_Win_fence"          ,0,0.0,0,0.0,0.0,0},
  {"MPI_Win_start"          ,0,0.0,0,0.0,0.0,0},
  {"MPI_Win_complete"       ,0,0.0,0,0.0,0.0,0},
  {"MPI_Win_post"           ,0,0.0,0,0.0,0.0,0},
  {"MPI_Win_wait"           ,0,0.0,0,0.0,0.0,0},
  {"MPI_Win_lock"           ,0,0.0,0,0.0,0.0,0},
  {"MPI_Win_lock_all"       ,0,0.0,0,0.0,0.0,0},
  {"MPI_Win_unlock"         ,0,0.0,0,0.0,0.0,0},
  {"MPI_Win_unlock_all"     ,0,0.0,0,0.0,0.0,0},
  {"MPI_Win_flush"          ,0,0.0,0,0.0,0.0,0},
  {"MPI_Win_flush_local"    ,0,0.0,0,0.0,0.0,0},
  {"MPI_Win_flush_all"        ,0,0.0,0,0.0,0.0,0},
  {"MPI_Win_flush_local_all"  ,0,0.0,0,0.0,0.0,0},
  {"MPI_Win_sync"             ,0,0.0,0,0.0,0.0,0},
  {"MPI_Neighbor_allgather"   ,0,0.0,0,0.0,0.0,0},
  {"MPI_Neighbor_allgatherv"  ,0,0.0,0,0.0,0.0,0},
  {"MPI_Neighbor_alltoall"    ,0,0.0,0,0.0,0.0,0},
  {"MPI_Neighbor_alltoallv"   ,0,0.0,0,0.0,0.0,0},
  {"MPI_Neighbor_alltoallw"   ,0,0.0,0,0.0,0.0,0},
  {"MPI_Ineighbor_allgather"  ,0,0.0,0,0.0,0.0,0},
  {"MPI_Ineighbor_allgatherv" ,0,0.0,0,0.0,0.0,0},
  {"MPI_Ineighbor_alltoall"   ,0,0.0,0,0.0,0.0,0},
  {"MPI_Ineighbor_alltoallv"  ,0,0.0,0,0.0,0.0,0},
  {"MPI_Ineighbor_alltoallw"  ,0,0.0,0,0.0,0.0,0},
  {NULL                       ,0,0.0,0,0.0,0.0,0}
};
// clang-format on

static FILE* listfile = NULL;

static int buffer_sizes[33];
static int root_buffer_sizes[33];

// static void buf_size_stat(int bsize)
// {
//   int nbits=0;
//   if(bsize <= 0) return;
//   while(bsize & ~0xFF) { nbits+=8 ; bsize >>=8; }
//   while(bsize & ~0x07) { nbits+=3 ; bsize >>=3; }
//   while(bsize & ~0x01) { nbits+=1 ; bsize >>=1; }
//   if(nbits>32) return;
//   buffer_sizes[nbits]++;
// }

void BufSizeMpiStat(int bsize) {
  int nbits = 0;
  if (bsize <= 0)
    return;
  while (bsize & ~0xFF) {
    nbits += 8;
    bsize >>= 8;
  }
  while (bsize & ~0x07) {
    nbits += 3;
    bsize >>= 3;
  }
  while (bsize & ~0x01) {
    nbits += 1;
    bsize >>= 1;
  }
  if (nbits > 32)
    return;
  buffer_sizes[nbits]++;
}

static void print_buf_size_stat(char* mesg, int* buffer_sizes) {
  int i, start;
  start = 1;
  for (i = 0; i < 30; i++) {
    if (buffer_sizes[i] > 0) {
      fprintf(listfile, "%s%9d buffers of size %9d -> %9d\n", mesg, buffer_sizes[i], start, start * 2 - 1);
    }
    start *= 2;
  }
}

// static int find_table_entry(const char *name){
//   int i=0;
//   while(stat_table[i].name != NULL){
//     if(strcmp(name,stat_table[i].name)==0) return i;
//     i++;
//   }
//   return -1;
// }

int FindMpiStatsEntry(const char* name) {
  int i = 0;
  while (stat_table[i].name != NULL) {
    if (strcmp(name, stat_table[i].name) == 0)
      return i;
    i++;
  }
  return -1;
}

// static void add_to_entry(int me,int bytes,int commsize,double time){
//   float rbytes;
//   if(me<0)return;
//   pmpi_r_statistics.time+=time;
//   pmpi_r_statistics.calls++;
//   stat_table[me].calls++;
//   stat_table[me].sbytes+=bytes;
//   stat_table[me].time+=time;
//   rbytes=bytes;
//   stat_table[me].sbytes2+=(rbytes*rbytes);
//   if(commsize>1){   /* for collectives only */
//     rbytes=rbytes*commsize;
//     stat_table[me].sbytes_coll+=rbytes;
//     stat_table[me].sbytes_coll2+=(rbytes*rbytes);
//   }
// }

void AddToMpiStatsEntry(int me, int bytes, int commsize, double time) {
  float rbytes;
  if (me < 0)
    return;
  pmpi_r_statistics.time += time;
  pmpi_r_statistics.calls++;
  stat_table[me].calls++;
  stat_table[me].sbytes += bytes;
  stat_table[me].time += time;
  rbytes = bytes;
  stat_table[me].sbytes2 += (rbytes * rbytes);
  if (commsize > 1) { /* for collectives only */
    rbytes = rbytes * commsize;
    stat_table[me].sbytes_coll += rbytes;
    stat_table[me].sbytes_coll2 += (rbytes * rbytes);
  }
}

void ResetMpiStats() {
  int i = 0;
  while (stat_table[i].name != NULL) {
    stat_table[i].sbytes       = 0;
    stat_table[i].calls        = 0;
    stat_table[i].time         = 0;
    stat_table[i].sbytes2      = 0;
    stat_table[i].sbytes_coll  = 0;
    stat_table[i].sbytes_coll2 = 0;
    i++;
  }
}

void CloseMpiStats() {
  fprintf(
      listfile, "INFO: exiting from profiling layer "
                "MPI_Init...\n====================================================================\n");
  if (listfile != stdout)
    fclose(listfile);
}

void ReadableByteCount(double num_bytes, char* buffer) {
  double amount = num_bytes;
  int    unit   = 0;

  const char UNITS[] = {'\0', 'k', 'M', 'G'};

  while (amount > 1900.0 && unit < 3) {
    amount /= 1024.0;
    unit++;
  }

  if (unit == 0)
    sprintf(buffer, "%7.0f", amount);
  else
    sprintf(buffer, "%6.1f%c", amount, UNITS[unit]);
}

static int print_stats = 3;   // print on all PEs by default ( 0 = no printing) (1|2 = print only on PE 0)

void SetDumpMpiStats(int flag){
  print_stats = flag ;
}

void set_dump_mpi_stats__(int* flag) {
  SetDumpMpiStats(*flag) ;
}

void set_dump_mpi_stats_(int* flag){
  SetDumpMpiStats(*flag) ;
}

void set_dump_mpi_stats(int* flag){
  SetDumpMpiStats(*flag) ;
}

void DumpMpiStats() {
  int       my_rank = -1;
  int       size    = 1;
  int       i       = 0;
  double    AVG     = 0.0;
  double    tmax, tmin, tmean;
  int       tcalls;
  long long tbytes;
  int print_flag ;

  char sbytes[32];
  char sbytes_coll[32];
  char avg_s[32];
  char tbytes_s[32];
  char bytes_per_sec_s[32];
  char *envar = getenv("DUMP_MPI_STATS") ;

  PMPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  PMPI_Comm_size(MPI_COMM_WORLD, &size);
  if(envar != NULL) print_stats = atoi(envar) ;
  print_flag = print_stats ;
  if((my_rank >  0) && (print_stats < 3)) print_flag = 0 ;   // all PEs only print if print_stats > 1
  if((my_rank == 0) && (print_stats > 0)) print_flag = 1 ;   // print everything on PE 0
  if((my_rank == 0) && (print_stats > 1)) print_flag = 2 ;   // print everything on PE 0

  while (stat_table[i].name != NULL) {
    if (stat_table[i].calls > 0) {
      AVG = stat_table[i].sbytes;
      AVG = AVG / stat_table[i].calls;
      ReadableByteCount(AVG, avg_s);
      ReadableByteCount((double)stat_table[i].sbytes, sbytes);
      ReadableByteCount((double)stat_table[i].sbytes_coll, sbytes_coll);
      ReadableByteCount((double)stat_table[i].sbytes / stat_table[i].time, bytes_per_sec_s);
      if(print_flag > 1)fprintf(
          listfile, "%5.5d: %-24s %8d calls %s [%s] bytes (avg %s), %10.3f seconds, %s/s\n", my_rank,
          stat_table[i].name, stat_table[i].calls, sbytes, sbytes_coll, avg_s, stat_table[i].time, bytes_per_sec_s);
    }
    i++;
  }
  if(print_flag > 1)fprintf(
      listfile, "%5.5d: SUMMARY %12d calls, needing %10.3f seconds\n", my_rank, pmpi_r_statistics.calls,
      pmpi_r_statistics.time);
  i = 0;
  while (stat_table[i].name != NULL) {
    PMPI_Allreduce(&stat_table[i].calls, &tcalls, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD);
    PMPI_Allreduce(&stat_table[i].sbytes, &tbytes, 1, MPI_INTEGER8, MPI_SUM, MPI_COMM_WORLD);
    PMPI_Allreduce(&stat_table[i].time, &tmax, 1, MPI_REAL8, MPI_MAX, MPI_COMM_WORLD);
    PMPI_Allreduce(&stat_table[i].time, &tmin, 1, MPI_REAL8, MPI_MIN, MPI_COMM_WORLD);
    PMPI_Allreduce(&stat_table[i].time, &tmean, 1, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD);
    if ((my_rank == 0) && (tcalls > 0) && (print_flag > 0)) {
      ReadableByteCount((double)tbytes, tbytes_s);
      fprintf(
          listfile, "TOTAL: %-24s %12d calls %s bytes, min/max/avg= %7.3f/%9.3f/%10.5f seconds\n", stat_table[i].name,
          tcalls, tbytes_s, tmin, tmax, tmean / size);
    }
    i++;
  }
  if(print_flag > 0) print_buf_size_stat("local: ", buffer_sizes);
  PMPI_Allreduce(buffer_sizes, root_buffer_sizes, 33, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD);
  if ((my_rank == 0) && (print_flag > 0))
    print_buf_size_stat("TOTAL: ", root_buffer_sizes);
  return;
}

void InitMpiStats() {
  char  fname[4096];
  char* Fname = &fname[0];
  char* envfile;
  char* mode    = "w";
  int   my_rank = -1;

  PMPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  if (my_rank == 0)
    fprintf(stderr, "INFO: entering profiling layer MPI_Init...\n");
  envfile = getenv("PMPI_OUT_FILE");
  if (envfile != NULL) {
    sprintf(Fname, "%s_%5.5d", envfile, my_rank);
    if (*Fname == '+') {
      Fname++;
      mode = "a";
    }
    listfile = fopen(Fname, mode);
  }
  if (listfile == NULL)
    listfile = stdout;
  if (my_rank == 0)
    fprintf(stderr, "INFO: entering profiling layer MPI_Init...\n");
}

#define MAXWINS 2048

static MPI_Win winlist[MAXWINS] ;      // windows list
static int     winrank[MAXWINS] ;      // ranks in window
static int init = 1 ;                  // initialize flag
static int used = 0 ;                  // number of registered windows in list


// insert/remove C MPI 1 sided window into/from tracked list
// TODO: add rank tracking (so that message at end is printed only by rank 0)
//       add rank << 1 to insert
void MPI_Track_Window(
  MPI_Win win,              // MPI C one sided window
  int insert)               // 1 = insert, 0 = remove
{
  int i ;
  if(init){                 // initialize list entries to null windows
    for(i = 0 ; i < MAXWINS ; i++) winlist[i] = MPI_WIN_NULL ;
    init = 0 ;
  }
  if(DEBUG) printf("DEBUG: MPI_Track_Window, insert= %d, used = %d \n", insert, used);
  if(insert & 1){                                          // insert

    for(i = 0 ; i < used ; i++){
      if(winlist[i] == win){                           // already in list
      if(DEBUG) printf("DEBUG: entry %d is a duplicate\n",i);
        return ;
      }
    }
    for(i = 0 ; i < used ; i++){
      if(winlist[i] == MPI_WIN_NULL){                  // resuse a free entry in list
        winlist[i] = win ;
        winrank[i] = insert >> 1 ;                        // keep rank
      if(DEBUG) printf("DEBUG: reusing entry %d\n", i);
        return ;
      }
    }
    if(used >= MAXWINS) return ;                       // table full, OUCH
    winlist[used] = win ;                              // add a new entry
    winrank[i] = insert >> 1 ;                            // keep rank
    if(DEBUG) printf("DEBUG: adding entry %d\n", used);
    used++ ;
    return ;

  }else{                                               // remove

    for(i = 0 ; i < used ; i++){
      if(winlist[i] == win){                           // entry found in table, replace with null window
        winlist[i] = MPI_WIN_NULL ;
        winrank[i] = 0 ;
        if(DEBUG) printf("removing entry %d\n", i);
        return ;
      }
    }
  }

}

// insert/remove Fortran MPI 1 sided window into/from tracked list
void MPI_Track_Window_f(MPI_Fint win_f, int insert){
  MPI_Win win = MPI_Win_f2c(win_f) ;                   // translate Fortran MPI window into C window
  MPI_Track_Window(win, insert) ;
  if(DEBUG) printf("DEBUG: (MPI_Track_Window_f) used = %d, insert = %d \n", used, insert);
}

// free all registered windows not freed yet
void MPI_Free_Tracked_Windows(){
  int i, count ;
  count = 0 ;
//   if(DEBUG) printf("MPI_Free_Tracked_Windows : start\n");
  if(used == 0) return ;
  for(i = 0 ; i < used ; i++){
    if(winlist[i] != MPI_WIN_NULL){                       // entry still valid
      if(winrank[i] == 0) printf("INFO: (MPI_Track_Window_f) freeing (unclosed) window at index %d\n",i);
      if(winrank[i] == 0) count++ ;
      PMPI_Win_free( &(winlist[i]) ) ;
      winlist[i] = MPI_WIN_NULL ;
    }
  }
  used = 0 ;
  if(count > 0) printf("INFO: (MPI_Free_Tracked_Windows) freed %d windows\n", count) ;
}

// manage functions to be called before MPI_Finalize

#define MAXFNS 32

typedef void (*fnptr)(void) ;
static fnptr fn_list[MAXFNS] ;
static int used_fns = 0 ;

// TODO: check for duplicates
// register a function to be executed before calling MPI_Finalize
int MPI_At_Finalize( void (*fn)(void) ){
  if(used_fns < MAXFNS){
    fn_list[used_fns] = fn ;           // store in function list if not full
  }else{
    return 1 ;                         // table is full
  }
  used_fns++ ;
  return 0 ;
}

// execute functions registered with MPI_At_Finalize (in reverse order of registration)
// NOTE: redundant calls become effective NO-OPs
void MPI_At_Finalize_exec(){
  while(used_fns > 0){
    used_fns-- ;
    fn_list[used_fns]() ;
  }
}
