//   Uuseful routines for C and FORTRAN programming
//   Copyright (C) 2020  Division de Recherche en Prevision Numerique
//                       Environnement Canada
// 
//   This is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation,
//   version 2.1 of the License.
// 
//   This software is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
// Author : M.Valin, Recherche en Prevision Numerique,   2020/2021
//          V.Magnoux, Recherche en Prevision Numerique, 2020/2021
//
// Fortran specific code
// Fortran name mangling assumed : one underscore added to name
//
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <mpi.h>

#if ! defined(DEBUG)
#define DEBUG 0
#endif

void InitMpiStats();
void DumpMpiStats();
void CloseMpiStats();
void BufSizeMpiStat(int bsize);
int FindMpiStatsEntry(const char *name);
void AddToMpiStatsEntry(int me,int bytes,int commsize,double time);

void MPI_Free_Tracked_Windows() ;
int MPI_At_Finalize( void (*fn)(void) );
void MPI_At_Finalize_exec();

// basic macros for Fortran interface support (straight or Fortran 2008 flavor)

#if defined(MPIF08)
#define FORTRAN_VERSION "( use mpi_f08 )"
#else
#define FORTRAN_VERSION "( use mpi / include 'mpif.h') "
#endif

#if ! defined(MPIF08)
  #define TRACK_WINDOWS 1
  #define FFMPI(x,X)   mpi_##x##_
  #define FFMPI__(x,X) mpi_##x##_
  #define FPMPI(x,X)   pmpi_##x##_
  #define FPMPI08(x,X) pmpi_##x##_
  #define FPMPITS(x,X) pmpi_##x##_
#endif

//OpenMPI 4.1.1
#if defined(USE_OPENMPI)
  #undef(USE_MPICH)
#endif

// Mpich 3.4.2
#if defined(USE_MPICH)
  #undef USE_OPENMPI

  #if defined(MPIF08)
    #define FFMPI(x,X)   mpi_##x##_f08_
    #define FFMPI__(x,X) mpi_##x##_
    #define TRACK_WINDOWS 1
    #define FPMPI(x,X)   pmpir_##x##_f08_
    #define FPMPI__(x,X) pmpi_##x##_
    #define FPMPITS(x,X) pmpir_##x##_f08ts_
  #endif
#else
  #define USE_OPENMPI

  #if defined(MPIF08)
    #define FFMPI(x,X)   mpi_##x##_f08_
    #define FFMPI__(x,X) mpi_##x##_
    #define TRACK_WINDOWS 1
    #define FPMPI(x,X)   pmpi_##x##_
    #define FPMPI__(x,X) pmpi_##x##_
    #define FPMPITS(x,X) pmpi_##x##_
  #endif
#endif

// nothing is needed for non Fortran 2008 Mpich profiling

#if defined(USE_OPENMPI) || defined(MPIF08)

// to deal with Fortran optional argument ierr

static long long optional ;         // target used in calls if argument is not present
static void *optptr = &optional ;

#define OPTIONAL(ptr) ((ptr == NULL) ? optptr : ptr )

// prototypes for ubiquitous functions in Fortran interface

void FPMPI(type_size,TYPE_SIZE)(int *datatype, int *dsize, int *ierr);
void FPMPI(comm_size,COMM_SIZE)(int *comm, int *size, int *ierr);
void FPMPI(comm_rank,COMM_RANK)(int *comm, int *rank, int *ierr);

// MPI init (Fortran and C)

void FPMPI(init,INIT)(int *ierr);
void FFMPI(init,INIT)(int *ierr)
{
  double     t0;
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Init Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Init");
  FPMPI(init,INIT)( OPTIONAL(ierr) );
  t0 = PMPI_Wtime();
  InitMpiStats();
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
  MPI_At_Finalize(&MPI_Free_Tracked_Windows) ;         // register function to auto free one sided windows not freed yet
}

void FPMPI(init_thread, INIT_THREAD)(int *required, int *provided, int *ierr);
void FFMPI(init_thread, INIT_THREAD)(int *required, int *provided, int *ierr)
{
  double     t0;
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Init_thread Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Init_thread");
  FPMPI(init_thread, INIT_THREAD)(required, provided, OPTIONAL(ierr) );
  t0 = PMPI_Wtime();
  InitMpiStats();
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
  MPI_At_Finalize(&MPI_Free_Tracked_Windows) ;         // register function to auto free one sided windows not freed yet
}

#if ! defined(USE_MPICH)

// MPI recv (Fortran and C)

void FPMPITS(recv,RECV)(void *buf, int *count, int *datatype, int *source, int *tag,
             int *comm, int *status, int *ierr);
void FFMPI(recv,RECV)(void *buf, int *count, int *datatype, int *source, int *tag,
             int *comm, int *status, int *ierr)
{
  int dsize;
  double t0;
  static int me=-1;

if(DEBUG) printf("DEBUG: MPI_Recv Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Recv");
  FPMPI(type_size,TYPE_SIZE)(datatype, &dsize, OPTIONAL(ierr) );
  t0=PMPI_Wtime();
  FPMPITS(recv,RECV)(buf,count,datatype,source,tag,comm,status,ierr);
  AddToMpiStatsEntry(me,(*count)*dsize,1,PMPI_Wtime()-t0);
  BufSizeMpiStat((*count)*dsize);
}
// MPI irecv (Fortran and C)

void FPMPITS(irecv,IRECV)(void *buf, int *count, int *datatype, int *source, int *tag,
             int *comm, int *request, int *ierr);

void FFMPI(irecv,IRECV)(void *buf, int *count, int *datatype, int *source, int *tag,
             int *comm, int *request, int *ierr)
{
  int dsize;
  double t0;
  static int me=-1;

if(DEBUG) printf("DEBUG: MPI_Irecv Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Irecv");
  FPMPI(type_size,TYPE_SIZE)(datatype, &dsize, OPTIONAL(ierr) );
  t0=PMPI_Wtime();
  FPMPITS(irecv,IRECV)(buf,count,datatype,source,tag,comm,request,ierr);
  AddToMpiStatsEntry(me,(*count)*dsize,1,PMPI_Wtime()-t0);
  BufSizeMpiStat((*count)*dsize);
}
// MPI send (Fortran and C)

void FPMPITS(send,SEND)(void *buf, int *count, int *datatype, int *dest, int *tag,
             int *comm, int *ierr);

void FFMPI(send,SEND)(void *buf, int *count, int *datatype, int *dest, int *tag,
             int *comm, int *ierr)
{
  int dsize;
  double t0;
  static int me=-1;

if(DEBUG) printf("DEBUG: MPI_Send Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Send");
  FPMPI(type_size,TYPE_SIZE)(datatype, &dsize, OPTIONAL(ierr) );
  t0=PMPI_Wtime();
  FPMPITS(send,SEND)(buf,count,datatype,dest,tag,comm,ierr);
  AddToMpiStatsEntry(me,(*count)*dsize,1,PMPI_Wtime()-t0);
  BufSizeMpiStat((*count)*dsize);
}
// MPI isend (Fortran and C)

void FPMPITS(isend,ISEND)(void *buf, int *count, int *datatype, int *dest, int *tag,
              int *comm, int *request, int *ierr);
void FFMPI(isend,ISEND)(void *buf, int *count, int *datatype, int *dest, int *tag,
              int *comm, int *request, int *ierr)
{
  int dsize;
  double t0;
  static int me=-1;

if(DEBUG) printf("DEBUG: MPI_Isend Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Isend");
  FPMPI(type_size,TYPE_SIZE)(datatype, &dsize, OPTIONAL(ierr) );
  t0=PMPI_Wtime();
  FPMPITS(isend,ISEND)(buf,count,datatype,dest,tag,comm,request,ierr);
  AddToMpiStatsEntry(me,(*count)*dsize,1,PMPI_Wtime()-t0);
  BufSizeMpiStat((*count)*dsize);
}
// MPI reduce (Fortran and C)

void FPMPITS(reduce,REDUCE)(void *sendbuf, void *recvbuf, int *count,
            int *datatype, int *op, int *root, int *comm, int *ierr);
void FFMPI(reduce,REDUCE)(void *sendbuf, void *recvbuf, int *count,
            int *datatype, int *op, int *root, int *comm, int *ierr)
{
  int dsize, size;
  double t0;
  static int me=-1;
  
if(DEBUG) printf("DEBUG: MPI_Reduce Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Reduce");
  FPMPI(type_size,TYPE_SIZE)(datatype, &dsize, OPTIONAL(ierr) );
  FPMPI(comm_size,COMM_SIZE)(comm, &size, OPTIONAL(ierr) );
  t0=PMPI_Wtime();
  FPMPITS(reduce,REDUCE)(sendbuf,recvbuf,count,datatype,op,root,comm,ierr);
  AddToMpiStatsEntry(me,(*count)*dsize,size,PMPI_Wtime()-t0);
  BufSizeMpiStat((*count)*dsize);
}
// MPI allreduce (Fortran and C)

void FPMPITS(allreduce,ALLREDUCE)(void *sendbuf, void *recvbuf, int *count,
            int *datatype, int *op, int *comm, int *ierr);
void FFMPI(allreduce,ALLREDUCE)(void *sendbuf, void *recvbuf, int *count,
            int *datatype, int *op, int *comm, int *ierr)
{
  int dsize, size;
  double t0;
  static int me=-1;

if(DEBUG) printf("DEBUG: MPI_Allreduce Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Allreduce");
  FPMPI(type_size,TYPE_SIZE)(datatype, &dsize, OPTIONAL(ierr) );
  FPMPI(comm_size,COMM_SIZE)(comm, &size, OPTIONAL(ierr) );
  t0=PMPI_Wtime();
  FPMPITS(allreduce,ALLREDUCE)(sendbuf,recvbuf,count,datatype,op,comm,ierr);
  AddToMpiStatsEntry(me,(*count)*dsize,size,PMPI_Wtime()-t0);
  BufSizeMpiStat((*count)*dsize);
}
// MPI broadcast (Fortran and C)

void FPMPITS(bcast,BCAST)(void* buffer, int *count, int *datatype, int *root, int *comm, int *ierr);
void FFMPI(bcast,BCAST)(void* buffer, int *count, int *datatype, int *root, int *comm, int *ierr)
{
  int dsize, size;
  double t0;
  static int me=-1;

if(DEBUG) printf("DEBUG: MPI_Bcast Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Bcast");
  FPMPI(type_size,TYPE_SIZE)(datatype, &dsize, OPTIONAL(ierr) );
  FPMPI(comm_size,COMM_SIZE)(comm, &size, OPTIONAL(ierr) );
  t0=PMPI_Wtime();
  FPMPITS(bcast,BCAST)(buffer, count, datatype, root, comm, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me,(*count)*dsize,size,PMPI_Wtime()-t0);
  BufSizeMpiStat((*count)*dsize);
}

#endif

// MPI barrier

void FPMPI(barrier,BARRIER)( int *comm, int *ierr);
void FFMPI(barrier,BARRIER)( int *comm, int *ierr)
{
  double t0;
  static int me=-1;

if(DEBUG) printf("DEBUG: MPI_barrier Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Barrier");
  t0=PMPI_Wtime();
  FPMPI(barrier,BARRIER)(comm, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me,0,1,PMPI_Wtime()-t0);
}
// MPI testall

void FPMPI(testall,TESTALL)(int *count,int *array_of_requests,int *flag,int *array_of_statuses, int *ierr);
void FFMPI(testall,TESTALL)(int *count,int *array_of_requests,int *flag,int *array_of_statuses, int *ierr)
{
  double t0;
  static int me=-1;
  
if(DEBUG) printf("DEBUG: MPI_Testall Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Testall");
  t0=PMPI_Wtime();
  FPMPI(testall,TESTALL)(count,array_of_requests,flag,array_of_statuses,ierr);
  AddToMpiStatsEntry(me,0,1,PMPI_Wtime()-t0);
}
// MPI wait

void FPMPI(wait,WAIT)(int *array_of_requests,int *array_of_statuses, int *ierr);
void FFMPI(wait,WAIT)(int *array_of_requests,int *array_of_statuses, int *ierr)
{
  double t0;
  static int me=-1;
  
if(DEBUG) printf("DEBUG: MPI_Wait Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Wait");
  t0=PMPI_Wtime();
  FPMPI(wait,WAIT)(array_of_requests,array_of_statuses,ierr);
  AddToMpiStatsEntry(me,0,1,PMPI_Wtime()-t0);
}

// MPI waitall

void FPMPI(waitall,WAITALL)(int *count,int *array_of_requests,int *array_of_statuses, int *ierr);
void FFMPI(waitall,WAITALL)(int *count,int *array_of_requests,int *array_of_statuses, int *ierr)
{
  double t0;
  static int me=-1;
  
if(DEBUG) printf("DEBUG: MPI_Waitall Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Waitall");
  t0=PMPI_Wtime();
  FPMPI(waitall,WAITALL)(count,array_of_requests,array_of_statuses,ierr);
  AddToMpiStatsEntry(me,0,1,PMPI_Wtime()-t0);
}

#if ! defined(USE_MPICH)

// MPI sendrecv

void FPMPITS(sendrecv,SENDRECV)(void *sendbuf, int *sendcount, int *sendtype,
            int *dest, int *sendtag, void *recvbuf, int *recvcount,
            int *recvtype, int *source, int *recvtag,
            int *comm, int *Status, int *ierr);
void FFMPI(sendrecv,SENDRECV)(void *sendbuf, int *sendcount, int *sendtype,
            int *dest, int *sendtag, void *recvbuf, int *recvcount,
            int *recvtype, int *source, int *recvtag,
            int *comm, int *Status, int *ierr)
{
  int rsize,ssize;
  double t0;
  static int me=-1;

if(DEBUG) printf("DEBUG: MPI_Sendrecv Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Sendrecv");
  FPMPI(type_size,TYPE_SIZE)(sendtype, &ssize, OPTIONAL(ierr) );
  FPMPI(type_size,TYPE_SIZE)(recvtype, &rsize, OPTIONAL(ierr) );
  t0=PMPI_Wtime();
  FPMPITS(sendrecv,SENDRECV)(sendbuf,sendcount,sendtype,dest,sendtag,recvbuf,recvcount,recvtype,source,recvtag,comm,Status,ierr);
  AddToMpiStatsEntry(me,(*sendcount)*ssize+(*recvcount)*rsize,1,PMPI_Wtime()-t0);
  BufSizeMpiStat((*sendcount)*ssize);
  BufSizeMpiStat((*recvcount)*rsize);
}
// MPI scatter

void FPMPITS(scatter,SCATTER)(void *sendbuf, int *sendcnt, int *sendtype, 
                            void *recvbuf, int *recvcnt, int *recvtype,  int *root, int *comm, int *ierr);
void FFMPI(scatter,SCATTER)(void *sendbuf, int *sendcnt, int *sendtype, 
                            void *recvbuf, int *recvcnt, int *recvtype,  int *root, int *comm, int *ierr)
{
  int rsize,ssize;
  double t0;
  static int me=-1;
  int size;
  int rank;

if(DEBUG) printf("DEBUG: MPI_Scatter Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Scatter");
  FPMPI(comm_size,COMM_SIZE)(comm,&size,ierr);
  FPMPI(comm_rank,COMM_RANK)(comm,&rank,ierr);
  FPMPI(type_size,TYPE_SIZE)(sendtype, &ssize, OPTIONAL(ierr) );
  FPMPI(type_size,TYPE_SIZE)(recvtype, &rsize, OPTIONAL(ierr) );
  t0=PMPI_Wtime();
  FPMPITS(scatter,SCATTER)(sendbuf,sendcnt,sendtype,recvbuf,recvcnt,recvtype,root,comm,ierr);
  if(rank != *root){
    AddToMpiStatsEntry(me,(*recvcnt)*rsize,1,PMPI_Wtime()-t0);
    BufSizeMpiStat((*recvcnt)*rsize);
  }else{
    AddToMpiStatsEntry(me,(*sendcnt)*ssize*size,1,PMPI_Wtime()-t0);
    while(size--) BufSizeMpiStat((*sendcnt)*ssize);
  }
}

// MPI scatterv

void FPMPITS(scatterv,SCATTERV)(void *sendbuf, int *sendcnts, int *displs, int *sendtype, 
                              void *recvbuf, int *recvcnt, int *recvtype, int *root, int *comm, int *ierr);
void FFMPI(scatterv,SCATTERV)(void *sendbuf, int *sendcnts, int *displs, int *sendtype, 
                              void *recvbuf, int *recvcnt, int *recvtype, int *root, int *comm, int *ierr)
{
  int rsize,ssize;
  double t0;
  static int me=-1;
  int size;
  int rank;
  int sendcnt, i;

if(DEBUG) printf("DEBUG: MPI_Scatterv Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Scatterv");
  FPMPI(comm_size,COMM_SIZE)(comm,&size,ierr);
  FPMPI(comm_rank,COMM_RANK)(comm,&rank,ierr);
  FPMPI(type_size,TYPE_SIZE)(sendtype, &ssize, OPTIONAL(ierr) );
  FPMPI(type_size,TYPE_SIZE)(recvtype, &rsize, OPTIONAL(ierr) );
  t0=PMPI_Wtime();
  FPMPITS(scatterv,SCATTERV)(sendbuf,sendcnts,displs,sendtype,recvbuf,recvcnt,recvtype,root,comm,ierr);
  if(rank == *root) {
    sendcnt=0;
    for (i=0;i<size;i++) sendcnt+=sendcnts[i];
    AddToMpiStatsEntry(me,sendcnt*ssize,1,PMPI_Wtime()-t0);
    while(size--) BufSizeMpiStat(sendcnts[i]*rsize);
  }else{
    AddToMpiStatsEntry(me,(*recvcnt)*rsize,1,PMPI_Wtime()-t0);
    BufSizeMpiStat((*recvcnt)*rsize);
  }
}

// MPI gather

void FPMPITS(gather,GATHER)(void *sendbuf, int *sendcnt, int *sendtype, 
                          void *recvbuf, int *recvcnt, int *recvtype,  int *root, int *comm, int *ierr);
void FFMPI(gather,GATHER)(void *sendbuf, int *sendcnt, int *sendtype, 
                          void *recvbuf, int *recvcnt, int *recvtype,  int *root, int *comm, int *ierr)
{
  int rsize,ssize;
  double t0;
  static int me=-1;
  int size;
  int rank;

if(DEBUG) printf("DEBUG: MPI_Gather Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Gather");
  FPMPI(comm_size,COMM_SIZE)(comm,&size,ierr);
  FPMPI(comm_rank,COMM_RANK)(comm,&rank,ierr);
  FPMPI(type_size,TYPE_SIZE)(sendtype, &ssize, OPTIONAL(ierr) );
  FPMPI(type_size,TYPE_SIZE)(recvtype, &rsize, OPTIONAL(ierr) );
  t0=PMPI_Wtime();
  FPMPITS(gather,GATHER)(sendbuf,sendcnt,sendtype,recvbuf,recvcnt,recvtype,root,comm,ierr);
  if(rank == *root) {
    AddToMpiStatsEntry(me,(*recvcnt)*rsize*size,1,PMPI_Wtime()-t0);
    while(size--) BufSizeMpiStat((*recvcnt)*rsize);
  }else{
    AddToMpiStatsEntry(me,(*sendcnt)*ssize,1,PMPI_Wtime()-t0);
    BufSizeMpiStat((*sendcnt)*ssize);
  }
}

// MPI gatherv

void FPMPITS(gatherv,GATHERV)(void *sendbuf, int *sendcnt, int *sendtype, void *recvbuf, 
                            int *recvcnts, int *displs, int *recvtype, int *root, int *comm, int *ierr);
void FFMPI(gatherv,GATHERV)(void *sendbuf, int *sendcnt, int *sendtype, void *recvbuf, 
                            int *recvcnts, int *displs, int *recvtype, int *root, int *comm, int *ierr)
{
  int rsize,ssize;
  double t0;
  static int me=-1;
  int size;
  int rank;
  int recvcnt, i;

if(DEBUG) printf("DEBUG: MPI_Gatherv Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Gatherv");
  FPMPI(comm_size,COMM_SIZE)(comm,&size,ierr);
  FPMPI(comm_rank,COMM_RANK)(comm,&rank,ierr);
  FPMPI(type_size,TYPE_SIZE)(sendtype, &ssize, OPTIONAL(ierr) );
  FPMPI(type_size,TYPE_SIZE)(recvtype, &rsize, OPTIONAL(ierr) );
  t0=PMPI_Wtime();
  FPMPITS(gatherv,GATHERV)(sendbuf,sendcnt,sendtype,recvbuf,recvcnts,displs,recvtype,root,comm,ierr);
  if(rank == *root) {
    recvcnt=0;
    for (i=0;i<size;i++) recvcnt+=recvcnts[i];
    AddToMpiStatsEntry(me,recvcnt*rsize,1,PMPI_Wtime()-t0);
    while(size--) BufSizeMpiStat(recvcnts[i]*rsize);
  }else{
    AddToMpiStatsEntry(me,(*sendcnt)*ssize,1,PMPI_Wtime()-t0);
    BufSizeMpiStat((*sendcnt)*ssize);
  }
}

// MPI alltoall

void FPMPITS(alltoall,ALLTOALL)(void *sendbuf, int *sendcount, int *sendtype, 
                             void *recvbuf, int *recvcount, int *recvtype, int *comm, int *ierr);
void FFMPI(alltoall,ALLTOALL)(void *sendbuf, int *sendcount, int *sendtype, 
                             void *recvbuf, int *recvcount, int *recvtype, int *comm, int *ierr)
{
  int rsize,ssize;
  double t0;
  static int me=-1;
  int size;
  
if(DEBUG) printf("DEBUG: MPI_Alltoall Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Alltoall");
  FPMPI(comm_size,COMM_SIZE)(comm,&size,ierr);
  FPMPI(type_size,TYPE_SIZE)(sendtype, &ssize, OPTIONAL(ierr) );
  FPMPI(type_size,TYPE_SIZE)(recvtype, &rsize, OPTIONAL(ierr) );
  t0=PMPI_Wtime();
  FPMPITS(alltoall,ALLTOALL)(sendbuf,sendcount,sendtype,recvbuf,recvcount,recvtype,comm,ierr);
  AddToMpiStatsEntry(me,(*sendcount)*ssize+(*recvcount)*rsize,size,PMPI_Wtime()-t0);
  while(size--) { BufSizeMpiStat((*sendcount)*ssize); BufSizeMpiStat((*recvcount)*rsize); }
}

// MPI alltoallv

void FPMPITS(alltoallv,ALLTOALLV)(void *sendbuf, int *sendcnts, int *sdispls, int *sendtype, 
                                void *recvbuf, int *recvcnts, int *rdispls, int *recvtype, int *comm, int *ierr);
void FFMPI(alltoallv,ALLTOALLV)(void *sendbuf, int *sendcnts, int *sdispls, int *sendtype, 
                                void *recvbuf, int *recvcnts, int *rdispls, int *recvtype, int *comm, int *ierr)
{
  int rsize,ssize;
  double t0;
  static int me=-1;
  int size;
  int sendcnt,recvcnt,i;
  
if(DEBUG) printf("DEBUG: MPI_Alltoallv Fortran %s\n", FORTRAN_VERSION);
  if(me==-1) me=FindMpiStatsEntry("MPI_Alltoallv");
  FPMPI(comm_size,COMM_SIZE)(comm,&size,ierr);
  FPMPI(type_size,TYPE_SIZE)(sendtype, &ssize, OPTIONAL(ierr) );
  FPMPI(type_size,TYPE_SIZE)(recvtype, &rsize, OPTIONAL(ierr) );
  recvcnt=0;
  for (i=0;i<size;i++) { recvcnt+=recvcnts[i]; BufSizeMpiStat(recvcnts[i]*rsize) ; }
  sendcnt=0;
  for (i=0;i<size;i++) { sendcnt+=sendcnts[i]; BufSizeMpiStat(sendcnts[i]*ssize) ; }
  t0=PMPI_Wtime();
  FPMPITS(alltoallv,ALLTOALLV)(sendbuf,sendcnts,sdispls,sendtype,recvbuf,recvcnts,rdispls,recvtype,comm,ierr);
  AddToMpiStatsEntry(me,sendcnt*ssize+recvcnt*rsize,1,PMPI_Wtime()-t0);
}

////////////////////////////
// One-sided communication
void FPMPITS(get, GET)(void *origin_buf, int *origin_count, int *origin_datatype, int *target_rank, MPI_Aint *target_disp,
                     int *target_count, int *target_datatype, int *window, int *ierr);
void FFMPI(get, GET)(void *origin_buf, int *origin_count, int *origin_datatype, int *target_rank, MPI_Aint *target_disp,
                     int *target_count, int *target_datatype, int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Get Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Get");

  int dsize;
  FPMPI(type_size, TYPE_SIZE)(origin_datatype, &dsize, OPTIONAL(ierr) );
  const double t0 = PMPI_Wtime();
  FPMPITS(get, GET)(origin_buf, origin_count, origin_datatype, target_rank, target_disp, target_count, target_datatype, window, OPTIONAL(ierr) );

  AddToMpiStatsEntry(me, (*origin_count) * dsize, 1, PMPI_Wtime() - t0);
  BufSizeMpiStat((*origin_count) * dsize);
}

void FPMPITS(put, PUT)(const void *origin_buf, int *origin_count, int *origin_datatype, int *target_rank,
                     MPI_Aint *target_disp, int *target_count, int *target_datatype, int *window, int *ierr);
void FFMPI(put, PUT)(const void *origin_buf, int *origin_count, int *origin_datatype, int *target_rank,
                     MPI_Aint *target_disp, int *target_count, int *target_datatype, int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Put Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Put");

  int dsize;
  FPMPI(type_size, TYPE_SIZE)(origin_datatype, &dsize, OPTIONAL(ierr) );
  const double t0 = PMPI_Wtime();
  FPMPITS(put, PUT)(origin_buf, origin_count, origin_datatype, target_rank, target_disp, target_count, target_datatype, window, OPTIONAL(ierr) );

  AddToMpiStatsEntry(me, (*origin_count) * dsize, 1, PMPI_Wtime() - t0);
  BufSizeMpiStat((*origin_count) * dsize);
}

void FPMPITS(accumulate, ACCUMULATE)(const void *origin_addr, int *origin_count, int *origin_datatype,
                                   int *target_rank, MPI_Aint *target_disp, int *target_count,
                                   int *target_datatype, int *operation, int *window, int *ierr);
void FFMPI(accumulate, ACCUMULATE)(const void *origin_addr, int *origin_count, int *origin_datatype,
                                   int *target_rank, MPI_Aint *target_disp, int *target_count,
                                   int *target_datatype, int *operation, int *window, int *ierr)
{
    static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Accumulate Fortran %s\n", FORTRAN_VERSION);
    if (me == -1)
        me = FindMpiStatsEntry("MPI_Accumulate");

    int dsize;
    FPMPI(type_size, TYPE_SIZE)(origin_datatype, &dsize, OPTIONAL(ierr) );
    const double t0 = PMPI_Wtime();
    FPMPITS(accumulate, ACCUMULATE)(origin_addr, origin_count, origin_datatype, target_rank,
            target_disp, target_count, target_datatype, operation, window, OPTIONAL(ierr) );
    AddToMpiStatsEntry(me, (*origin_count) * dsize, 1, PMPI_Wtime() - t0);
    BufSizeMpiStat((*origin_count) * dsize);
}

void FPMPITS(get_accumulate, GET_ACCUMULATE)(const void *origin_addr, int *origin_count, int *origin_datatype,
        void *result_addr, int *result_count, int *result_datatype, int *target_rank, MPI_Aint *target_disp,
        int *target_count, int *target_datatype, int *operation, int *window, int *ierr);
void FFMPI(get_accumulate, GET_ACCUMULATE)(const void *origin_addr, int *origin_count, int *origin_datatype,
        void *result_addr, int *result_count, int *result_datatype, int *target_rank, MPI_Aint *target_disp,
        int *target_count, int *target_datatype, int *operation, int *window, int *ierr)
{
    static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Get_accumulate Fortran %s\n", FORTRAN_VERSION);
    if (me == -1)
        me = FindMpiStatsEntry("MPI_Get_accumulate");

    int dsize;
    FPMPI(type_size, TYPE_SIZE)(origin_datatype, &dsize, OPTIONAL(ierr) );
    const double t0 = PMPI_Wtime();
    FPMPITS(get_accumulate, GET_ACCUMULATE)(
            origin_addr, origin_count, origin_datatype, result_addr, result_count, result_datatype,
            target_rank, target_disp, target_count, target_datatype, operation, window, OPTIONAL(ierr) );
    AddToMpiStatsEntry(me, (*origin_count) * dsize, 1, PMPI_Wtime() - t0);
    BufSizeMpiStat((*origin_count) * dsize);
}

#endif

void FPMPI(win_fence, WIN_FENCE)(int *assert, int *window, int *ierr);
void FFMPI(win_fence, WIN_FENCE)(int *assert, int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Win_fence Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_fence");

  const double t0 = PMPI_Wtime();
  FPMPI(win_fence, WIN_FENCE)(assert, window, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_start, WIN_START)(int *group, int *assert, int *window, int *ierr);
void FFMPI(win_start, WIN_START)(int *group, int *assert, int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Win_start Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_start");

  const double t0 = PMPI_Wtime();
  FPMPI(win_start, WIN_START)(group, assert, window, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_complete, WIN_COMPLETE)(int *window, int *ierr);
void FFMPI(win_complete, WIN_COMPLETE)(int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Win_complete Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_complete");

  const double t0 = PMPI_Wtime();
  FPMPI(win_complete, WIN_COMPLETE)(window, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_post, WIN_POST)(int *group, int *assert, int *window, int *ierr);
void FFMPI(win_post, WIN_POST)(int *group, int *assert, int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Win_post Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_post");

  const double t0 = PMPI_Wtime();
  FPMPI(win_post, WIN_POST)(group, assert, window, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_wait, WIN_WAIT)(int *window, int *ierr);
void FFMPI(win_wait, WIN_WAIT)(int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Win_wait Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_wait");

  const double t0 = PMPI_Wtime();
  FPMPI(win_wait, WIN_WAIT)(window, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_lock, WIN_LOCK)(int *lock_type, int *rank, int *assert, int *window, int *ierr);
void FFMPI(win_lock, WIN_LOCK)(int *lock_type, int *rank, int *assert, int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Win_lock Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_lock");

  const double t0 = PMPI_Wtime();
  FPMPI(win_lock, WIN_LOCK)(lock_type, rank, assert, window, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_lock_all, WIN_LOCK_ALL)(int *assert, int *window, int *ierr);
void FFMPI(win_lock_all, WIN_LOCK_ALL)(int *assert, int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Win_lock_all Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_lock_all");

  const double t0 = PMPI_Wtime();
  FPMPI(win_lock_all, WIN_LOCK_ALL)(assert, window, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_unlock, WIN_UNLOCK)(int *rank, int *window, int *ierr);
void FFMPI(win_unlock, WIN_UNLOCK)(int *rank, int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Win_unlock Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_unlock");

  const double t0 = PMPI_Wtime();
  FPMPI(win_unlock, WIN_UNLOCK)(rank, window, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_unlock_all, WIN_UNLOCK_ALL)(int *window, int *ierr);
void FFMPI(win_unlock_all, WIN_UNLOCK_ALL)(int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Win_unlock_all Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_unlock_all");

  const double t0 = PMPI_Wtime();
  FPMPI(win_unlock_all, WIN_UNLOCK_ALL)(window, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_flush, WIN_FLUSH)(int *rank, int *window, int *ierr);
void FFMPI(win_flush, WIN_FLUSH)(int *rank, int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Win_flush Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_flush");

  const double t0 = PMPI_Wtime();
  FPMPI(win_flush, WIN_FLUSH)(rank, window, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_flush_local, WIN_FLUSH_LOCAL)(int *rank, int *window, int *ierr);
void FFMPI(win_flush_local, WIN_FLUSH_LOCAL)(int *rank, int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Win_flush_local Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_flush_local");

  const double t0 = PMPI_Wtime();
  FPMPI(win_flush_local, WIN_FLUSH_LOCAL)(rank, window, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_flush_all, WIN_FLUSH_ALL)(int *window, int *ierr);
void FFMPI(win_flush_all, WIN_FLUSH_ALL)(int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Win_flush_all Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_flush_all");

  const double t0 = PMPI_Wtime();
  FPMPI(win_flush_all, WIN_FLUSH_ALL)(window, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_flush_local_all, WIN_FLUSH_LOCAL_ALL)(int *window, int *ierr);
void FFMPI(win_flush_local_all, WIN_FLUSH_LOCAL_ALL)(int *window, int *ierr)
{
  static int me = -1;
if(DEBUG) printf("DEBUG: MPI_Win_flush_local_all Fortran %s\n", FORTRAN_VERSION);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_flush_local_all");

  const double t0 = PMPI_Wtime();
  FPMPI(win_flush_local_all, WIN_FLUSH_LOCAL_ALL)(window, OPTIONAL(ierr) );
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void MPI_Track_Window_f(MPI_Fint win_f, int insert) ;

// subroutine MPI_WIN_ALLOCATE(SIZE, DISP_UNIT, INFO, COMM, BASEPTR, WIN, IERROR)    ! include 'mpif.h'  |  use mpi
//   use :: MPI_tracked_windows_mod
//   USE, INTRINSIC :: ISO_C_BINDING, ONLY : C_INTPTR_T
//   INTEGER(KIND=C_INTPTR_T) SIZE, BASEPTR
//   INTEGER :: DISP_UNIT, INFO, COMM, WIN, IERROR
//   integer :: my_rank
//   call PMPI_COMM_RANK(COMM, my_rank, IERROR)
//   my_rank = my_rank + my_rank
//   if(DEBUG > 0) print *,'DEBUG: MPI_WIN_ALLOCATE Fortran'
//   call PMPI_WIN_ALLOCATE(SIZE, DISP_UNIT, INFO, COMM, BASEPTR, WIN, IERROR)
//   call MPI_Track_Window(WIN, my_rank + 1)  ! insert
// end subroutine MPI_WIN_ALLOCATE
void FPMPI(win_allocate,WIN_ALLOCATE)(intptr_t *size, int *disp_unit, int *info, int *comm, intptr_t *baseptr, int *window, int *ierr);
void FFMPI(win_allocate,WIN_ALLOCATE)(intptr_t *size, int *disp_unit, int *info, int *comm, intptr_t *baseptr, int *window, int *ierr)
{
  int rank;
  FPMPI(comm_rank,COMM_RANK)(comm,&rank,OPTIONAL(ierr));
  rank = rank + rank ;
  if(DEBUG) printf("MPI_Win_allocate Fortran %s\n", FORTRAN_VERSION);
  FPMPI(win_allocate,WIN_ALLOCATE)(size, disp_unit, info, comm, baseptr, window, OPTIONAL(ierr));
  if(TRACK_WINDOWS) MPI_Track_Window_f(*window, rank + 1);
}

// subroutine MPI_WIN_ALLOCATE_SHARED(SIZE, DISP_UNIT, INFO, COMM, BASEPTR, WIN, IERROR)    ! include 'mpif.h'  |  use mpi
//   use :: MPI_tracked_windows_mod
//   USE, INTRINSIC :: ISO_C_BINDING, ONLY : C_INTPTR_T
//   INTEGER(KIND=C_INTPTR_T) SIZE, BASEPTR
//   INTEGER DISP_UNIT, INFO, COMM, WIN, IERROR
//   integer :: my_rank
//   call PMPI_COMM_RANK(COMM, my_rank, IERROR)
//   my_rank = my_rank + my_rank
//   if(DEBUG > 0) print *,'DEBUG: MPI_WIN_ALLOCATE_SHARED Fortran'
//   call PMPI_WIN_ALLOCATE_SHARED(SIZE, DISP_UNIT, INFO, COMM, BASEPTR, WIN, IERROR)
//   call MPI_Track_Window(WIN, my_rank + 1)  ! insert
// end subroutine MPI_WIN_ALLOCATE_SHARED
void FPMPI(win_allocate_shared,WIN_ALLOCATE_SHARED)(intptr_t *size, int *disp_unit, int *info, int *comm, intptr_t *baseptr, int *window, int *ierr);
void FFMPI(win_allocate_shared,WIN_ALLOCATE_SHARED)(intptr_t *size, int *disp_unit, int *info, int *comm, intptr_t *baseptr, int *window, int *ierr)
{
  int rank;
  FPMPI(comm_rank,COMM_RANK)(comm,&rank,OPTIONAL(ierr));
  rank = rank + rank ;
  if(DEBUG) printf("MPI_Win_allocate_shared Fortran %s\n", FORTRAN_VERSION);
  FPMPI(win_allocate_shared,WIN_ALLOCATE_SHARED)(size, disp_unit, info, comm, baseptr, window, OPTIONAL(ierr));
  if(TRACK_WINDOWS) MPI_Track_Window_f(*window, rank + 1);
}

// subroutine MPI_WIN_CREATE(BASE, SIZE, DISP_UNIT, INFO, COMM, WIN, IERROR)    ! include 'mpif.h'  |  use mpi
//   use :: MPI_tracked_windows_mod
//   USE, INTRINSIC :: ISO_C_BINDING, ONLY : C_INTPTR_T
//   INTEGER BASE(*)
//   INTEGER(KIND=C_INTPTR_T) SIZE
//   INTEGER DISP_UNIT, INFO, COMM, WIN, IERROR
//   integer :: my_rank
//   call PMPI_COMM_RANK(COMM, my_rank, IERROR)
//   my_rank = my_rank + my_rank
//   if(DEBUG > 0) print *,'DEBUG: MPI_WIN_CREATE Fortran'
//   call PMPI_WIN_CREATE(BASE, SIZE, DISP_UNIT, INFO, COMM, WIN, IERROR)
//   call MPI_Track_Window(WIN, my_rank + 1)  ! insert
// end subroutine MPI_WIN_CREATE

#if ! defined(USE_MPICH)

void FPMPITS(win_create,WIN_CREATE)(void *base, intptr_t *size, int *disp_unit, int *info, int *comm, int *window, int *ierr);
void FFMPI(win_create,WIN_CREATE)(void *base, intptr_t *size, int *disp_unit, int *info, int *comm, int *window, int *ierr)
{
  int rank;
  FPMPI(comm_rank,COMM_RANK)(comm,&rank,OPTIONAL(ierr));
  rank = rank + rank ;
  if(DEBUG) printf("MPI_Win_create Fortran %s\n", FORTRAN_VERSION);
  FPMPITS(win_create,WIN_CREATE)(base, size, disp_unit, info, comm, window, OPTIONAL(ierr));
  if(TRACK_WINDOWS) MPI_Track_Window_f(*window, rank + 1);
}

#endif

// subroutine MPI_WIN_CREATE_DYNAMIC(INFO, COMM, WIN, IERROR)   ! include 'mpif.h'  |  use mpi
//   use :: MPI_tracked_windows_mod
//   INTEGER INFO, COMM, WIN, IERROR
//   integer :: my_rank
//   call PMPI_COMM_RANK(COMM, my_rank, IERROR)
//   my_rank = my_rank + my_rank
//   if(DEBUG > 0) print *,'DEBUG: MPI_WIN_CREATE_DYNAMIC Fortran'
//   call PMPI_WIN_CREATE_DYNAMIC(INFO, COMM, WIN, IERROR)
//   call MPI_Track_Window(WIN, my_rank + 1)  ! insert
// end subroutine MPI_WIN_CREATE_DYNAMIC
void FPMPI(win_create_dynamic,WIN_CREATE_DYNAMIC)(int *info, int *comm, int *window, int *ierr);
void FFMPI(win_create_dynamic,WIN_CREATE_DYNAMIC)(int *info, int *comm, int *window, int *ierr)
{
  int rank;
  FPMPI(comm_rank,COMM_RANK)(comm,&rank,OPTIONAL(ierr));
  rank = rank + rank ;
  if(DEBUG) printf("MPI_Win_create_dynamic Fortran %s\n", FORTRAN_VERSION);
  FPMPI(win_create_dynamic,WIN_CREATE_DYNAMIC)(info, comm, window, OPTIONAL(ierr));
  if(TRACK_WINDOWS) MPI_Track_Window_f(*window, rank + 1);
}

// subroutine MPI_WIN_FREE(WIN, IERROR)
//   use :: MPI_tracked_windows_mod
//   INTEGER :: WIN, IERROR
//   if(DEBUG > 0) print *,'DEBUG: MPI_WIN_FREE Fortran',win
//   call MPI_Track_Window(WIN, 0)  ! remove from tables
//   call PMPI_WIN_FREE(WIN, IERROR)
// end subroutine MPI_WIN_FREE
void FPMPI(win_free,WIN_FREE)(int *window, int *ierr);
void FFMPI(win_free,WIN_FREE)(int *window, int *ierr)
{
  if(DEBUG) printf("MPI_Win_free Fortran %s\n", FORTRAN_VERSION);
  if(TRACK_WINDOWS) MPI_Track_Window_f(*window, 0);
  FPMPI(win_free,WIN_FREE)(window, OPTIONAL(ierr));
}

// MPI finalize and associate (Fortran and C)

void FFMPI(finalize,FINALIZE)(int *ierr)
{
  if(DEBUG) printf("MPI_Fnalize Fortran %s\n", FORTRAN_VERSION);
  DumpMpiStats();
  CloseMpiStats();
  MPI_At_Finalize_exec();
  *ierr = PMPI_Finalize();
}

#endif
