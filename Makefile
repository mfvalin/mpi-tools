
# library for C profiling
LIBC = libmpitools
OBJECTSC = mpi_prof_common.o mpi_prof_c.o

# library for Fortran profiling (includes C profiling)
LIBF = libmpitools_f
OBJECTSF = mpi_prof_common.o mpi_prof_fortran.o mpi_prof_c.o mpi_prof_fortran_f08.o

CC = mpicc
FC = mpif90

ifdef DEBUG
DBG = -DDEBUG
endif

ifdef MPICH
USE_MPICH = -DUSE_MPICH
endif

MPIFLAGS = 

# little extra for gfortran
ifdef GNU
EXTRAF = -fallow-argument-mismatch -ffree-line-length-0
endif
FFLAGS = -fPIC -O $(EXTRAF) -I. $(DBG) $(USE_MPICH)
CFLAGS = -fPIC -O -Wall -g $(DBG) $(USE_MPICH)

all: $(LIBC).a $(LIBC).so $(LIBF).a test_f.Abs test_f2008.Abs

libso: $(LIBF).so

test_f.Abs: $(LIBF).a 
	$(FC) test_mpi_finalize.F90 -L. -lmpitools_f -o $@

test_f2008.Abs: $(LIBF).a 
	$(FC) test_mpi_finalize.F90 -L. -lmpitools_f -DMPIF08 -o $@

tests: all f_test.Abs f_test_dyn.Abs c_test.Abs c_test_dyn.Abs

# common code
mpi_prof_common.o: mpi_prof_common.c
	$(CC) $(MPIFLAGS) $(CFLAGS) -c $< -o $@

# C specific code
mpi_prof_c.o: mpi_prof_c.c
	$(CC) $(MPIFLAGS) $(CFLAGS) -c $< -o $@

# Fortran specific code
mpi_prof_fortran.o: mpi_prof_fortran.c
	$(CC) $(MPIFLAGS) $(CFLAGS) -c $< -o $@

# Fortran 2008 style specific code
mpi_prof_fortran_f08.o: mpi_prof_fortran.c
	$(CC) $(MPIFLAGS) $(CFLAGS) -DMPIF08 -c $< -o $@

# static library
$(LIBC).a: $(OBJECTSC)
	ar rcv $@ $(OBJECTSC)

# shared object, can also be used with LD_PRELOAD
$(LIBC).so: $(OBJECTSC)
	$(CC) -shared -o $@ $(OBJECTSC)

# static library
$(LIBF).a: $(OBJECTSF)
	ar rcv $@ $(OBJECTSF)

# shared object, can also be used with LD_PRELOAD
$(LIBF).so: $(OBJECTSF)
	$(CC) -shared -o $@ $(OBJECTSF)

# linked with static profiling library
c_test.Abs: c_test.c $(LIBC).a $(OBJECTSC)
	$(CC) $(CFLAGS) -o $@ c_test.c -Wl,-Bstatic -L. -lmpitools -Wl,-Bdynamic

# to be used with LD_PRELOAD
c_test_dyn.Abs: c_test.c $(LIBC).so
	$(CC) $(CFLAGS) -o $@ c_test.c

# linked with static profiling library
f_test.Abs: f_test.F90 $(LIBF).a $(OBJECTSF)
	$(FC) $(FFLAGS) -o $@ f_test.F90 -Wl,-Bstatic -L. -lmpitools_f -Wl,-Bdynamic

# to be used with LD_PRELOAD
f_test_dyn.Abs: f_test.F90 $(LIBF).so
	$(FC) $(FFLAGS) -o $@ f_test.F90

clean:
	rm -f *.o *.so *.a *.Abs *.mod a.out .nfs*
	rm -rf .fo
