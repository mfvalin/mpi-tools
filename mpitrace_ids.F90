! MIT License
! 
! Copyright (c) 2022 Environment Canada
! author : M.Valin Recherche en Prevision Numerique
! 
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
! 
! The above copyright notice and this permission notice shall be included in all
! copies or substantial portions of the Software.
! 
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
! SOFTWARE.
!
! this work is derived from and inspired by mpitrace 
! (Copyright (c) 2020 International Business Machines)
! https://github.com/IBM/mpitrace
module mpitrace_ids
  use ISO_C_BINDING
  use mpi_f08
  implicit none
  logical, save :: log_is_active = .false.

  ! /*----------------------------------------------------------*/
  ! /*            IDs for MPI functions                         */
  ! /*----------------------------------------------------------*/
  integer, parameter :: COMM_RANK_ID = 0
  integer, parameter :: COMM_SIZE_ID = 1
  integer, parameter :: SEND_ID = 2
  integer, parameter :: SSEND_ID = 3
  integer, parameter :: RSEND_ID = 4
  integer, parameter :: BSEND_ID = 5
  integer, parameter :: ISEND_ID = 6
  integer, parameter :: ISSEND_ID = 7
  integer, parameter :: IRSEND_ID = 8
  integer, parameter :: IBSEND_ID = 9
  integer, parameter :: SEND_INIT_ID = 10
  integer, parameter :: SSEND_INIT_ID = 11
  integer, parameter :: RSEND_INIT_ID = 12
  integer, parameter :: BSEND_INIT_ID = 13
  integer, parameter :: RECV_INIT_ID = 14
  integer, parameter :: RECV_ID = 15
  integer, parameter :: IRECV_ID = 16
  integer, parameter :: SENDRECV_ID = 17
  integer, parameter :: SENDRECV_REPLACE_ID = 18
  integer, parameter :: BUFFER_ATTACH_ID = 19
  integer, parameter :: BUFFER_DETACH_ID = 20
  integer, parameter :: PROBE_ID = 21
  integer, parameter :: IPROBE_ID = 22
  integer, parameter :: TEST_ID = 23
  integer, parameter :: TESTANY_ID = 24
  integer, parameter :: TESTALL_ID = 25
  integer, parameter :: TESTSOME_ID = 26
  integer, parameter :: WAIT_ID = 27
  integer, parameter :: WAITANY_ID = 28
  integer, parameter :: WAITALL_ID = 29
  integer, parameter :: WAITSOME_ID = 30
  integer, parameter :: START_ID = 31
  integer, parameter :: STARTALL_ID = 32
  integer, parameter :: BCAST_ID = 33
  integer, parameter :: IBCAST_ID = 34
  integer, parameter :: BARRIER_ID = 35
  integer, parameter :: IBARRIER_ID = 36
  integer, parameter :: REDUCE_ID = 37
  integer, parameter :: IREDUCE_ID = 38
  integer, parameter :: ALLREDUCE_ID = 39
  integer, parameter :: IALLREDUCE_ID = 40
  integer, parameter :: REDUCE_SCATTER_ID = 41
  integer, parameter :: IREDUCE_SCATTER_ID = 42
  integer, parameter :: REDUCE_SCATTER_BLOCK_ID = 43
  integer, parameter :: IREDUCE_SCATTER_BLOCK_ID = 44
  integer, parameter :: GATHER_ID = 45
  integer, parameter :: IGATHER_ID = 46
  integer, parameter :: GATHERV_ID = 47
  integer, parameter :: IGATHERV_ID = 48
  integer, parameter :: SCAN_ID = 49
  integer, parameter :: ISCAN_ID = 50
  integer, parameter :: EXSCAN_ID = 51
  integer, parameter :: IEXSCAN_ID = 52
  integer, parameter :: ALLGATHER_ID = 53
  integer, parameter :: NEIGHBOR_ALLGATHER_ID = 54
  integer, parameter :: IALLGATHER_ID = 55
  integer, parameter :: INEIGHBOR_ALLGATHER_ID = 56
  integer, parameter :: ALLGATHERV_ID = 57
  integer, parameter :: NEIGHBOR_ALLGATHERV_ID = 58
  integer, parameter :: IALLGATHERV_ID = 59
  integer, parameter :: INEIGHBOR_ALLGATHERV_ID = 60
  integer, parameter :: SCATTER_ID = 61
  integer, parameter :: ISCATTER_ID = 62
  integer, parameter :: SCATTERV_ID = 63
  integer, parameter :: ISCATTERV_ID = 64
  integer, parameter :: ALLTOALL_ID = 65
  integer, parameter :: NEIGHBOR_ALLTOALL_ID = 66
  integer, parameter :: IALLTOALL_ID = 67
  integer, parameter :: INEIGHBOR_ALLTOALL_ID = 68
  integer, parameter :: ALLTOALLV_ID = 69
  integer, parameter :: NEIGHBOR_ALLTOALLV_ID = 70
  integer, parameter :: IALLTOALLV_ID = 71
  integer, parameter :: INEIGHBOR_ALLTOALLV_ID = 72
  integer, parameter :: ALLTOALLW_ID = 73
  integer, parameter :: NEIGHBOR_ALLTOALLW_ID = 74
  integer, parameter :: IALLTOALLW_ID = 75
  integer, parameter :: INEIGHBOR_ALLTOALLW_ID = 76
  integer, parameter :: ACCUMULATE_ID = 77
  integer, parameter :: FETCH_AND_OP_ID = 78
  integer, parameter :: GET_ID = 79
  integer, parameter :: GET_ACCUMULATE_ID = 80
  integer, parameter :: PUT_ID = 81
  integer, parameter :: RACCUMULATE_ID = 82
  integer, parameter :: RGET_ID = 83
  integer, parameter :: RGET_ACCUMULATE_ID = 84
  integer, parameter :: RPUT_ID = 85
  integer, parameter :: WIN_ALLOCATE_ID = 86
  integer, parameter :: WIN_ALLOCATE_SHARED_ID = 87
  integer, parameter :: WIN_ATTACH_ID = 88
  integer, parameter :: WIN_COMPLETE_ID = 89
  integer, parameter :: WIN_CREATE_ID = 90
  integer, parameter :: WIN_CREATE_DYNAMIC_ID = 91
  integer, parameter :: WIN_DETACH_ID = 92
  integer, parameter :: WIN_FENCE_ID = 93
  integer, parameter :: WIN_FLUSH_ID = 94
  integer, parameter :: WIN_FLUSH_ALL_ID = 95
  integer, parameter :: WIN_FLUSH_LOCAL_ID = 96
  integer, parameter :: WIN_FLUSH_LOCAL_ALL_ID = 97
  integer, parameter :: WIN_FREE_ID = 98
  integer, parameter :: WIN_LOCK_ID = 99
  integer, parameter :: WIN_LOCK_ALL_ID = 100
  integer, parameter :: WIN_POST_ID = 101
  integer, parameter :: WIN_START_ID = 102
  integer, parameter :: WIN_SYNC_ID = 103
  integer, parameter :: WIN_TEST_ID = 104
  integer, parameter :: WIN_UNLOCK_ID = 105
  integer, parameter :: WIN_UNLOCK_ALL_ID = 106
  integer, parameter :: WIN_WAIT_ID = 107

  integer, parameter :: MIN_MPI_IO_ID = 108

  integer, parameter :: FILE_CLOSE_ID = 108
  integer, parameter :: FILE_DELETE_ID = 109
  integer, parameter :: FILE_IREAD_ID = 110
  integer, parameter :: FILE_IREAD_AT_ID = 111
  integer, parameter :: FILE_IREAD_SHARED_ID = 112
  integer, parameter :: FILE_IWRITE_ID = 113
  integer, parameter :: FILE_IWRITE_AT_ID = 114
  integer, parameter :: FILE_IWRITE_SHARED_ID = 115
  integer, parameter :: FILE_OPEN_ID = 116
  integer, parameter :: FILE_PREALLOCATE_ID = 117
  integer, parameter :: FILE_READ_ID = 118
  integer, parameter :: FILE_READ_ALL_ID = 119
  integer, parameter :: FILE_READ_ALL_BEGIN_ID = 120
  integer, parameter :: FILE_READ_ALL_END_ID = 121
  integer, parameter :: FILE_READ_AT_ID = 122
  integer, parameter :: FILE_READ_AT_ALL_ID = 123
  integer, parameter :: FILE_READ_AT_ALL_BEGIN_ID = 124
  integer, parameter :: FILE_READ_AT_ALL_END_ID = 125
  integer, parameter :: FILE_READ_ORDERED_ID = 126
  integer, parameter :: FILE_READ_ORDERED_BEGIN_ID = 127
  integer, parameter :: FILE_READ_ORDERED_END_ID = 128
  integer, parameter :: FILE_READ_SHARED_ID = 129
  integer, parameter :: FILE_SEEK_ID = 130
  integer, parameter :: FILE_SEEK_SHARED_ID = 131
  integer, parameter :: FILE_SET_VIEW_ID = 132
  integer, parameter :: FILE_SYNC_ID = 133
  integer, parameter :: FILE_WRITE_ID = 134
  integer, parameter :: FILE_WRITE_ALL_ID = 135
  integer, parameter :: FILE_WRITE_ALL_BEGIN_ID = 136
  integer, parameter :: FILE_WRITE_ALL_END_ID = 137
  integer, parameter :: FILE_WRITE_AT_ID = 138
  integer, parameter :: FILE_WRITE_AT_ALL_ID = 139
  integer, parameter :: FILE_WRITE_AT_ALL_BEGIN_ID = 140
  integer, parameter :: FILE_WRITE_AT_ALL_END_ID = 141
  integer, parameter :: FILE_WRITE_ORDERED_ID = 142
  integer, parameter :: FILE_WRITE_ORDERED_BEGIN_ID = 143
  integer, parameter :: FILE_WRITE_ORDERED_END_ID = 144
  integer, parameter :: FILE_WRITE_SHARED_ID = 145

  type, BIND(C) :: timeval   ! TOTALLY DISHONEST declaration, timedelta will eventually be C code
    private
    integer(C_INT64_T), dimension(2) :: t
  end type
  interface
    subroutine timeofday(tv, tz) BIND(C,name='gettimeofday')
      import :: timeval, C_PTR
      implicit none
      type(timeval), intent(OUT)     :: tv
      type(C_PTR), intent(IN), value :: tz
    end subroutine timeofday
    subroutine f_logevent(id, t1, t2, src, dest, bytes, comm) BIND(C, name='FLogEvent')
      import :: timeval, C_INT32_T, MPI_Comm
      implicit none
      type(timeval), intent(IN), value :: t1, t2
      integer, INTENT(IN), value :: comm
      integer(C_INT32_T), intent(IN), value :: id, src, dest, bytes
    end subroutine f_logevent
  end interface
contains
  function timedelta(t1, t2) result(usecs)
    implicit none
    type(timeval), intent(IN) :: t1, t2
    real(kind=4) :: usecs
    usecs = (t2%t(2) - t1%t(2)) * .000001
    usecs = usecs + (t2%t(1) - t1%t(1))
  end function timedelta
  subroutine F08LogEvent(id, t1, t2, src, dest, dtype, nitems, com, collective, nvitems, nvtypes)
    implicit none
    integer, intent(IN), value                  :: id
    type(timeval), intent(IN)                   :: t1, t2
    integer, intent(IN), OPTIONAL               :: src, dest, nitems
    TYPE(MPI_Comm), INTENT(IN), OPTIONAL        :: com
    TYPE(MPI_Datatype), INTENT(IN), OPTIONAL    :: dtype
    logical, intent(IN), OPTIONAL               :: collective
    integer, intent(IN), dimension(*), OPTIONAL :: nvitems
    TYPE(MPI_Datatype), intent(IN), dimension(*), OPTIONAL :: nvtypes

    integer :: destval, srcval, bytesval, comval, dsize

    destval = -1 ; srcval = -1 ; bytesval = 0 ; comval = MPI_COMM_NULL%mpi_val ; dsize = 0
    if(present(dtype)) call MPI_Type_size(dtype, dsize)
    if(present(src)) srcval = src
    if(present(dest)) destval = dest
    if(present(nitems)) bytesval = nitems * dsize
    if(present(com)) comval = com%mpi_val
    call f_logevent(id, t1, t2, srcval, destval, bytesval, comval)
  end subroutine F08LogEvent
end module
